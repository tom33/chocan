# ChocAn

The project is about Chocoholics Anonymous (ChocAn), a group that aids those who are dependent on all types of chocolate. Members receive limitless consultations and treatments with medical professionals in exchange for a monthly charge. Providers use specially created computer terminals to validate the member's information and charge ChocAn for the services rendered. Members are issued plastic cards with magnetic strips containing their personal information. At the end of each week, accounting operations are carried out to provide reports for members and providers that include information about the services that were rendered, such as the date of the service, the names and addresses of the provider and member, the service codes, and the prices paid.

## Purpose and Scope

Purpose: The Chocoholics Anonymous (ChocAn) project's goal is to develop a computerized system that enables participants to pay a monthly fee and obtain limitless consultations and treatments from medical professionals like internists, nutritionists, and fitness specialists. The system checks member eligibility and bills for services using plastic cards with magnetic strips and terminals with specialized software. Additionally, the system creates reports for every member who consulted a provider throughout the week as well as for every physician who billed ChocAn during the week.

Scope: The project's scope comprises creating and implementing the system as well as designing and developing the software product. The project entails building a provider directory with service codes and costs, as well as a database of member and provider data. The system is made to manage a lot of data while yet being simple to use for both members and providers.

## Target Audience

Individuals or organizations that are members in the ChocAn.

## Terms and Definitions

Main term: ChocAn system

Definition: ChocAn system is an organization that provides healthcare services to its members addicted to chocolate by giving them unlimited consultations and treatments with healthcare professionals such as dietitians, internists, and exercise specialists. The system includes a plastic card with a magnetic strip and a nine-digit number that is given to the members to access the healthcare services. The providers of the services have a specially designed computer terminal with a card reader to verify the member number and bill for the services provided. The system also has a Provider Directory that lists all the service codes and fees for the providers to reference. The main accounting procedure is run at the ChocAn Data Center every week to generate reports that include member information, services provided, and fees to be paid to the providers.

# Product Overview

The ChocAn system is a computerized information system created to support the business operations of a fictitious organization that is dedicated to helping people addicted to chocolate in all its glorious forms. The system is intended to handle and keep current records of member and provider data as well as the services that providers offer to members. Additional tools for processing and submitting claims, paying providers, and producing different reports for management analysis are also included. The ChocAn system's objectives are to increase the organization's operational efficiency and accuracy while also raising the standard of care for its members.

## Users and Stakeholders

The Users and Stakeholders section's goal is to list the people, organizations, and groups that will be impacted by or involved in the project. It outlines the stakeholders who are concerned with the project's success as well as the users of the system, including their roles, responsibilities, and needs. This part aids in ensuring that the project team is fully aware of the target audience for the system they are creating as well as their needs and expectations.

## Use cases

The use cases section's goal is to give a thorough explanation of the numerous circumstances in which the ChocAn system will be put to use. It describes the steps users will take to use the system to complete particular tasks and reach their objectives.

### Example 1

![example](./assets/ChocAn%20usecase1.jpg)

### Example 2

![example](./assets/ChocAn%20usecase2.jpg)

# Functional Requirements

Outlined below are features and functions this system should provide, as well as how the system should behave in particular situations.

## Member Verification

Providers shall be able to verify each member by inputting their 9 digit member number into the terminal. If the number is valid, the word "Validated" will appear on the display. If the number is invalid, the reason shall be displayed, such as "Invalid Number" or "Member suspended".

## Billing ChocAn

Providers shall be able to bill ChocAn after a healthcare service has been provided by verifying the member, then inputting the date, appropriate 6 digit service code, and an optional comment about the service provided. The complete record is saved in a document. The software then looks up the fee to be paid for that service and displays it. For verification purposes, the provider will enter the current date and time, the date the service was provided, member name and number, service code, and fee to be paid into a form.

Providers shall be able to request the software for the Provider Directory, which shall be alphabetically ordered and contain the corresponding service codes and fees. The Provider Directory shall exist as a file.

## Weekly Reports

Each week, the system shall generate a weekly report for members and providers, detailing the services provided in order of service date. Each report also can be run individually at the request of a ChocAn manager at any time during the week. Each report shall be written to its own file, which should be named beginning with the member/provider name, followed by the date of the report. Table 1 details the differences between member and provider reports.

![weeklyreport](./assets/ChocAn-weeklyReport.png)
