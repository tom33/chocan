#include "structures.h"
Billing::Billing(){}

Billing::~Billing(){}

int Billing::create_billing(string date, int pro_num, int mem_num, int code, string new_comment) 
{
    time_t now = time(0);
    tm* dateTime = localtime(&now);
    strftime(currentDate, sizeof(currentDate), "%m-%d-%Y", dateTime);
    service_date = date;
    provider_number = pro_num;
    member_number = mem_num;
    service_code = code;
    comment.clear();
    comment = new_comment;
    return 1;
}

int Billing::remove_matching_billing()
{
    return 0;
}

int Billing::remove_all()
{
    return 0;
}

int Billing::display_all()
{
    cout << "Service Date: " << service_date << endl;
    cout << "Date Entered: " << currentDate << endl;
    cout << "Provider Number: " << provider_number << endl;
    cout << "Member Number: " << member_number << endl;
    cout << "Service Code: " << service_code << endl;
    cout << "Comments: " << comment << endl;
    cout << endl;
    return 0;
}

int Billing::display_matching_billing()
{
    return 0;
}