#include "structures.h"

#define MAX_DATE 11
Consultation::Consultation()
{
    current_date = nullptr;
    service_date = nullptr;
    comments = nullptr;
    member_number = 0;
    provider_number = 0;
    code = 0;
}

Consultation::~Consultation()
{
    if (current_date)
        delete[] current_date;
    if(service_date)
	delete [] service_date;
    if(comments)
	delete [] comments;
    current_date = nullptr;
    service_date = nullptr;
    comments = nullptr;
    member_number = 0;
    provider_number = 0;
    code = 0;
}

Consultation::Consultation(char a_current_date[], char a_service_date[], char a_comments[], int a_member_number, int a_provider_number, int a_code):
                            member_number(a_member_number), provider_number(a_provider_number), code(a_code)
{
    current_date = new char[strlen(a_current_date) + 1];
    strcpy(current_date, a_current_date);
    service_date = new char[strlen(a_service_date) + 1];
    strcpy(service_date, a_service_date);
    comments = new char[strlen(a_comments) +1];
    strcpy(comments, a_comments);
}

Consultation::Consultation(const Consultation & copy)
{
    current_date = new char[strlen(copy.current_date) + 1];
    strcpy(current_date, copy.current_date);
    service_date = new char[strlen(copy.service_date) + 1];
    strcpy(service_date, copy.service_date);
    comments = new char[strlen(copy.comments) +1];
    strcpy(comments, copy.comments);
    service_date = copy.service_date;
    comments = copy.comments;
    member_number = copy.member_number;
    provider_number = copy.provider_number;
    code = copy.code;
}

int Consultation::record_to_disk(const Consultation& src)
{
  ofstream outFile("consultation_records.txt", ios::app);
  if (outFile.is_open())
  {
    outFile << "Current Date and Time: " << src.current_date << '\t';
    outFile << "Date of Service: " << src.service_date << '\t';
    outFile << "Provider Number: " << src.provider_number << '\t';
    outFile << "Member Number: " << src.member_number << '\t';
    outFile << "Service Code: " << src.code << '\t';
    outFile << "Comments: " << src.comments << '\n';
      outFile.close();
      cout << "Consultation record successfully." << endl;

      return 1;
    }
  else
  {
    cout << "Unable to open the record disk" << endl;
    return 0;
  }
}


Consultation & Consultation::operator=(const Consultation & src)
{
    try
    {
        if(this == &src)
        {
            throw 10;
        }
    }
    catch(int x)
    {
        cout << "Can't copy itself." << endl;
    }
    if (current_date)
        delete [] current_date;
    if (service_date)
	delete [] service_date;
    if (comments)
	delete [] comments;
    current_date = new char[strlen(src.current_date) + 1];
    strcpy(current_date, src.current_date);
    service_date = new char[strlen(src.service_date) + 1];
    strcpy(service_date, src.service_date);
    comments = new char[strlen(src.comments) + 1];
    strcpy(comments, src.comments);
    member_number = src.member_number;
    provider_number = src.provider_number;
    code = src.code;
	
    return *this;
}

int Consultation::read_from_disk(char a_current_date[], char a_service_date[], char a_comments[], int a_member_number, int a_provider_number, int a_code)
{
  
  ifstream file_in;
  file_in.open("consultation_records.txt");
  if (file_in)
  {
    try
    {
      file_in.get(a_current_date, 11, '\t');
      file_in.ignore(100, '\t');
      while (!file_in.eof())
      {
        file_in.get(a_service_date, 11, '\t');
	file_in.ignore(100, '\t');
        file_in >> a_provider_number;
        file_in.ignore(100, '\t');
        file_in >> a_member_number;
        file_in.ignore(100, '\t');
        file_in >> a_code;
        file_in.ignore(100, '\t');
        file_in.get(a_comments, 100, '\n');
	file_in.ignore(100, '\n');
	Consultation consultation(a_current_date, a_service_date, a_comments, a_member_number, a_provider_number, a_code);
	Consultation_list.push_back(consultation);
	file_in.get(a_current_date, 11, '\t');
	file_in.ignore(100, '\t');	
      }
      file_in.close();
      return 1;
    }
    catch (const std::exception& ex)
    {
      cout << "An error occurred: " << ex.what() << endl;
      file_in.close();
      return 0;
    }
  }
  else
  {
    cout << "Unable to open the consultation records file" << endl;
    return 0;
  }
}

