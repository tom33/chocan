#include "structures.h"
#include <cstdlib>

void Member_Menu()
{
	cout << "Member menu:" << endl;
	cout << "1-Create a member account" << endl;
	cout << "2-Choose a service" << endl;
	cout << "3-Display information" << endl;
	cout << "4-Delete member acount" << endl;
	cout << "5-Generate report" << endl;
	cout << "6-Update Member Info\n";
	cout << "7-Quit" << endl;
}
int count_digits(int num){//counts how many digits are in 'num'
	return int(log10(num) + 1);
}

void Provider_Menu()
{
	cout << "Provider menu:" << endl;
	cout << "1-Create provider account" << endl;
	cout << "2-Display information" << endl;
	cout << "3-Delete provider account" << endl;
	cout << "4-Generate report" << endl;
	cout << "5-Create Bill" << endl;
	cout << "6-Display Bill" << endl;
	cout << "7-Update Provider Info\n";
	cout << "7-Quit" << endl;
}

int getChoice(int lo, int hi)//gets choice from user
{
	int choice = 0;
	cout << "Enter Choice: ";
	cin >> choice;
	cin.ignore(100, '\n');
	while(choice < lo || choice > hi)
	{
		cout << "Wrong choice, please enter again. ";
		cin >> choice;
		cin.ignore(100, '\n');
	}
	return choice;
}
int add_member(Member &, vector<Member> &);
int add_service(services &, Member &, Consultation &, provider);
int add_provider(provider &);
int remove_provider(provider &);
int menu();
int main(){
		
	menu();
	cout << "LOGGED OUT. THANKS FOR USING OUR SERVICES :)\n\n\n";
	return 1;	

}

int menu() 
{
	srand((unsigned) time(NULL));
	provider pro;
	services ser;
	Consultation con;
	Member mem;
	Billing bill;
	vector<Billing> bill_list;//list of bills
	vector<Member> member_list;//list of members
	bool cont = true;
	string name, member_name, provider_name, service_name, comments;
	string street_address, city, state;
	char service_date[11];
	int provider_number, member_number, service_code;
	int response;
	int input = 0; //user input 

	do{

		cout << "Are you a	(1) Member	(2) Provider	(3) Quit\n"
			<< "Enter in either 1  OR  2  OR  3\n";
		cin >> input; cin.ignore(100,'\n');
			
		if(input < 1 || input > 3)
			cout << "ERROR: DID NOT ENTER IN OPTION LISTED ABOVE\n\n";

	}while(input < 1 || input > 3);

	if(input == 1) //Member
	{
		while(cont)
		{
			Member_Menu();
			int choice = getChoice(1,7);
			cout << endl;
			if(choice == 1)//add member
			{
				add_member(mem, member_list);
			}
			else if(choice == 2) // add service
			{
				add_service(ser, mem, con, pro);
			}

			else if(choice == 3)//display member info
			{
				int num_dig, mem_id;
                        do{
                                cout << "Enter in the member ID you wish to display: ";
                                cin >> mem_id; cin.ignore(100,'\n');
                                num_dig = count_digits(mem_id);
                            if(num_dig != MAX_ID)
                                cout << "ERROR: MEMBER ID MUST BE 9 DIGITS. TRY AGAIN.\n\n";

                        }while(num_dig != MAX_ID);
                	mem.display_matching_member(mem_id);
			}

			else if(choice == 4)//delete member
			{
				int num_dig, mem_id;
                        do{
                                cout << "Enter in the member ID you wish to remove: ";
                                cin >> mem_id; cin.ignore(100,'\n');
                                num_dig = count_digits(mem_id);
                            if(num_dig != MAX_ID)
                                cout << "ERROR: MEMBER ID MUST BE 9 DIGITS. TRY AGAIN.\n\n";

                        }while(num_dig != MAX_ID);

                        mem.remove_member(mem_id);
			}
			else if(choice == 5)//generate report
			{
				mem.generateMemberServiceRerpot();
			}
			else if(choice == 6)//update member info
			{
				int check_id, num_digit;
				do{
					cout << "Enter in the member ID you'd like to update: ";
					cin >> check_id; cin.ignore(100,'\n');
					num_digit = count_digits(check_id);
					if(num_digit != MAX_ID)
						cout << "ERROR: PLEASE ENTER IN A 9 DIGIT MEMBER ID. TRY AGAIN\n\n";
				}while(num_digit != MAX_ID);
				int update_res=0;
				for(size_t i = 0; i < member_list.size(); ++i){
					update_res = member_list[i].update_member(check_id);
				}
				if(!update_res)
					cout << "Did not find member with that ID.\n\n";
			}
			else
				cont = false;
			
		}
	}
	else if(input == 2)//provider
	{
		while(cont)
		{
			Provider_Menu();
			int choice = getChoice(1,8);
			cout << endl;	
			if(choice == 1)//add provider
			{
				add_provider(pro);
			}
			
			else if(choice == 2)//display provider's info
			{
				int find_id, digit_count;
				do{

					cout << "Enter in your ID: ";
					cin >> find_id; cin.ignore(100,'\n');
					digit_count = count_digits(find_id);

					if(digit_count != 9)
						cout << "ERROR: PLEASE ENTER IN A 9 DIGIT ID. TRY AGAIN PLEASE.\n\n";
				}while(digit_count != MAX_ID);

				int result = pro.find_provider(find_id);
				if(result == 0)
					cout << "ERROR: PROVIDER ID ENTERED DOES NOT EXIST\n\n";

				else	
					pro.display_provider(find_id);
			}
			else if(choice == 3)
			{
				int num_count=0;
				do{
					cout << "Please enter the number of the provider account you want to delete: ";
					cin >> provider_number;
					cin.ignore(100, '\n');
					num_count = count_digits(provider_number);
					if(num_count != 9)
						cout << "ERROR: 9 DIGITS WERE NOT ENTERED. TRY AGAIN";
				}while(num_count != 9);

				pro.remove_provider(provider_number);
			}
			else if(choice == 4)
			{
				int num_count=0;
				do{
					cout << "Please enter your provider ID: ";
					cin >> provider_number;
					cin.ignore(100, '\n');
					num_count = count_digits(provider_number);
					if(num_count != 9)
						cout << "ERROR: 9 DIGITS WERE NOT ENTERED. TRY AGAIN";
				}while(num_count != 9);
				pro.generate_provider_report(provider_number);
			}
			else if(choice == 5)
			{
				cout << "Please enter the date that the service was provided (MMDDYYYY): ";
				cin.get(service_date, 11, '\n');
				cin.ignore(100, '\n');
				while(strlen(service_date) > 11)
				{
					cout << "The date is too long, please enter the date again(maximum 11 characters): ";
					cin.get(service_date, 11, '\n');
        				cin.ignore(100, '\n');
				}

				cout << "Please enter the 9 digit provider number: ";
			    	cin >> provider_number;
			    	cout << endl;
			    	cin.ignore(100, '\n');
				while(count_digits(provider_number) < MAX_ID || count_digits(provider_number) > MAX_ID)
				{
					cout << "Provider ID should be 9 digits long. Try Again." << endl;
					cout << "Please enter the 9 digit provider number: ";
					cin >> provider_number;
					cin.ignore(100, '\n');
				}

			    	cout << "Please enter the 9 digit member number: ";
				cin >> member_number;
			    	cout << endl;
			    	cin.ignore(100, '\n');
				while(count_digits(member_number) < MAX_ID || count_digits(member_number) > MAX_ID)
				{
					cout << "Member ID should be 9 digits long. Try Again. " << endl;
					cout << "Please enter the 9 digit member number: ";
					cin >> member_number;
					cin.ignore(100, '\n');
				}
			    	cout << "Please enter the 6 digit service code: ";
			    	cin >> service_code;
			    	cout << endl;
			    	cin.ignore(100, '\n');
				while(count_digits(service_code) < MAX_SERVICE_CODE || count_digits(service_code) > MAX_SERVICE_CODE)
				{
					cout << "Service code should be 6 digits long. Try Again." << endl;
					cout << "Please enter the 6 digit service code: ";
					cin >> service_code;
					cin.ignore(100, '\n');
				} 
			    	cout << "Would you like to enter a comment about the service?" << endl;
			    	cout << "1--Yes" << endl;
			    	cout << "2--No" << endl;
                		cin >> response;
                		cin.ignore(100, '\n');
                		while(response < 1 || response > 2)
                		{
                    			cout << "You can only enter 1 or 2, please choose again: ";
                    			cin >> response;
                    			cin.ignore(100, '\n');
                		}
                		if(response == 1)
                		{
                    			cout << "Please enter the comment (limit 100 characters): ";
                    			getline(cin,comments);
                    			while(comments.length() > MAX_COMMENTS)
                    			{
                        			cout << "The comment is too long, please enter again(limit 100 characters): ";
                        			comments.clear();
                        			getline(cin, comments);
                    			}
                		}
                
                		bill.create_billing(service_date, provider_number, member_number, service_code, comments);
                		bill_list.push_back(bill);
			    
			}
			else if (choice == 6)
			{
			    if (bill_list.size() == 0)
			    {
			        cout << "No bills yet." << endl;
			    }
			    else
			    {
			        for(size_t i = 0; i < bill_list.size(); ++i)
			        {
					bill_list[i].display_all();
				}
			    }
			}
			else if(choice == 7){//update provider

				int find_pro =0, count_pro;
				do{
					cout << "Enter in the provider ID you are wanting to update: ";
					cin >> find_pro; cin.ignore(100,'\n');

					count_pro = count_digits(find_pro);
					if(count_pro != MAX_ID)
						cout << "PROVIDER ID YOU ENTERED MUST BE 9 DIGITS. TRY AGAIN\n\n";
					
				}while(count_pro != MAX_ID);
				pro.update_provider(find_pro);


			}
			else
				cont = false;
			
			cout << endl;
		}
	}
	else if(input == 3)
		cont = false;
	do{
		cout << "Would you like to:\n(1) Continue\t(2)QUIT\nEnter in either 1  OR  2:   ";
		cin >> input; cin.ignore(100,'\n');
		
		if(input < 1 || input > 2)
			cout << "ERROR: DID NOT ENTER IN OPTION LISTED ABOVE\n\n";
	}while(input < 1 || input > 2);
	
	if(input == 1)
		return menu();

	return 1;
}

int add_member(Member & mem, vector<Member> & member_list){
	string name, street_address, city, state;
	int zipcode, id;
	bool status=0;

	cout << "Please enter your name(maximum 25 characters): ";
	getline(cin, name);
	while(name.length() > MAX_NAME)
	{
		cout << "The name is too long, please enter your name again(maximum 25 characters): ";
		name.clear();
		getline(cin,name);
	}

	cout << "Please enter your street address(maximum 25 characters): ";
	getline(cin, street_address);
	while(street_address.length() > MAX_ADDRESS)
	{
		cout << "The address is too long, please enter your address again(maximum 25 characters): ";
		street_address.clear();
		getline(cin, street_address);
	}

	cout << "Please enter your city(maximum 14 characters): ";
	getline(cin, city);
	while(city.length() > MAX_CITY)
	{
		cout << "The city is too long, please enter your city again(maximum 14 characters): ";
		city.clear();
		getline(cin,city);
	}
				
	cout << "Enter state(must be 2 characters): ";
	getline(cin, state);
	while(state.length() > MAX_STATE || state.length() < MAX_STATE)
	{
		cout << "The state is too long or too short, please enter your state again(must be 2 characters): ";
		state.clear();
		getline(cin,state);
	}
			
	int num_of_digits =0 ;
	do{
		cout << "Enter zipcode (5 CHARACTERS): ";
		cin >> zipcode; cin.ignore(100,'\n');
		num_of_digits = count_digits(zipcode);

		if(num_of_digits < MAX_ZIPCODE || num_of_digits > MAX_ZIPCODE)
			cout << "ERROR: ZIP CODE ENTERED IS EITHER TOO LONG OR TOO SHORT. TRY AGAIN\n\n";
	}while(num_of_digits > MAX_ZIPCODE || num_of_digits < MAX_ZIPCODE);
				
				
	id = 100000000 + (rand() % 900000001);
	mem.add_member(name, street_address, city, state, zipcode, id, status);
	cout << "your id is: " << id << endl;
	cout << "Added member successfully." << endl;
	//member_list.push_back(mem);
	return 1;
}

int add_service(services & ser, Member & mem, Consultation & con, provider pro){
	string service_name, member_name, provider_name;
	int code, id, provider_id;
	time_t now = time(0);
	char service_date[11];
	char comments[MAX_COMMENTS] = "";
    tm* dateTime = localtime(&now);
    char currentDate[20];
    strftime(currentDate, sizeof(currentDate), "%m-%d-%Y %H:%M:%S", dateTime);
	float fee =0.0f;
	int choice = 0;
	cout << "What service would you like to get? " 
	     << "Dietitian\tBlood test\tAerobics\tInternists\n";	
	getline(cin, service_name);
	while(service_name != "Dietitian" && service_name != "Blood test" && service_name != "Aerobics" && service_name != "Internists")
	{
		cout << "The service you entered is wrong, please ennter again. " << endl;
		cout << "What service would you like to get? "
	        	<< "(1) Dietitian\t(2) Blood test\t(3) Aerobics\t(4) Internists\n";
         	getline(cin, service_name);
	}
	if(service_name == "Dietitian")
		code = 598470;
	else if(service_name == "Blood test")
		code = 144127;
	else if(service_name == "Aerobics")
		code = 883948;
	else
		code = 456742;
	if(code == 598470)
		fee = DIETITIAN;
	else if(code == 144127)
		fee = BLOOD_TEST;
	else if(code == 883948)
		fee = AEROBICS;
	else
		fee = INTERNISTS;
	cout << "What date do you want to reserve? Enter digits in the format of MMDDYYYY: ";			
	cin.get(service_date, 11, '\n');
	cin.ignore(100, '\n');
	while(strlen(service_date) > 11)
	{
		cout << "The date is too long, please enter your name again(maximum 11 characters): ";
		cin.get(service_date, 11, '\n');
        	cin.ignore(100, '\n');
	}
	cout << "Please enter the member name(maximum 25 characters): ";
	getline(cin, member_name);
	while(member_name.length() > MAX_NAME)
	{
			cout << "The name is too long, please enter your name again(maximum 25 characters): ";
			member_name.clear();
			getline(cin,member_name);
	}

	cout << "Which provider do you want to have? ";
	pro.display_all_provider();
	getline(cin,provider_name);
	while(provider_name.length() > MAX_NAME)
	{
		cout << "The name is too long, please enter the provider name again(maximum 25 characters): ";
		provider_name.clear();
		getline(cin, provider_name);
	}
	cout << "Please enter provider id(9 digits): ";
	cin >> provider_id;
	cin.ignore(100, '\n');
	while(count_digits(provider_id) != MAX_ID)
	{
		cout << "The provider id you entered is too long or short, please enter again(9 digits): ";
		cin >> provider_id;
		cin.ignore(100, '\n');
	}
	//check with the provider list to see if it match or not
	//bool provider_check = comapre_provider(provider_id);
	//while(provider_check == false)
	//{
		//cout << "provider doesn't exist" << endl;
		//cout << "Please enter provider id: ";
		//cin >> provider_id;
		//cin.ignore(100, '\n');
	//}
	cout << "Please enter your id ";
	cin >> id; cin.ignore(100, '\n');
	while(count_digits(id) != MAX_ID)
	{
		cout << "The id you entered is too long or short, please enter again(9 digits): ";
		cin >> id;
		cin.ignore(100, '\n');
	}
	cout << "Do you want to enter any comment? (1-Yes) (2-No) ";
      	cin >> choice;
      	cin.ignore(100, '\n');

      while (choice < 1 || choice > 2)
      {
        cout << "You can only enter 1 or 2, please choose again: ";
        cin >> choice;
        cin.ignore(100, '\n');
      }

      if (choice == 1)
      {
        	cout << "Please enter the comments (maximum 100 characters): ";
        	cin.get(comments, MAX_COMMENTS, '\n');
		cin.ignore(100, '\n');

        	while (strlen(comments) >= MAX_COMMENTS)
        	{
          		cout << "The comment is too long, please enter again (maximum 100 characters): ";
          		cin.get(comments, MAX_COMMENTS, '\n');
	  		cin.ignore(100, '\n');
        	}
	}
		
	ser = services(service_name, service_date, member_name, provider_name, id, provider_id,  code, fee);
	cout << "Added service successfully." << endl;
	mem.add_service(ser);
	
	con = Consultation(currentDate, service_date, comments, id, provider_id, code);
	con.record_to_disk(con);
	cout << "Record to disk successfully. " << endl;
	return 1;
}

int add_provider(provider & pro){
	string name, street_address, city, state;
	int zipcode, provider_number;
	bool cont = true;
	while(cont)
	{
			cout << "Please enter your name(maximum 25 characters): ";
			getline(cin, name);
			while(name.length() > MAX_NAME)
			{
				cout << "The name is too long, please enter your name again(maximum 25 characters): ";
				name.clear();
				getline(cin, name);
			}
			cout << "Please enter your street address(maximum 25 characters): ";
			getline(cin,street_address);
			while(street_address.length() > MAX_ADDRESS)
			{
				cout << "The address is too long, please enter your address again(maximum 25 characters): ";
				street_address.clear();
				getline(cin, street_address);
			}
			cout << "Please enter your city(maximum 14 characters): ";
			getline(cin, city);
			while(city.length() > MAX_CITY)
			{
				cout << "The city is too long, please enter your city again(maximum 14 characters): ";
				city.clear();
				getline(cin, city);
			}
			cout << "Enter state(2 characters): ";
			getline(cin, state);
			while(state.length() > MAX_STATE || state.length() < MAX_STATE)
			{
				cout << "The state is too long or too short, please enter your state again(maximum 2 characters): ";
				state.clear();
				getline(cin, state);
			}
			int num_of_digits;
			do{
				cout << "Enter zipcode (5 CHARACTERS): ";
				cin >> zipcode; cin.ignore(100,'\n');
				num_of_digits = count_digits(zipcode);

				if(num_of_digits < MAX_ZIPCODE || num_of_digits > MAX_ZIPCODE)
					cout << "ERROR: ZIP CODE ENTERED IS EITHER TOO LONG OR TOO SHORT. TRY AGAIN\n\n";
			}while(num_of_digits > MAX_ZIPCODE || num_of_digits < MAX_ZIPCODE);	
			provider_number = 100000000 + (rand() % 900000001);
			pro.add_provider(name, street_address, city, state, zipcode, provider_number);
			cout << "Added provider successfully." << endl << endl;
			cont = false;
	}
	return 1;
}



