#include "structures.h"
#include <vector>

using std::vector;

Member::Member(const string a_name, const string a_street_address, const string a_city,
 const string a_state, int a_zipcode, int a_id, bool a_status):name(a_name), 
 street_address(a_street_address), city(a_city), state(a_state), zipcode(a_zipcode),
  id(a_id), status(a_status)
{}

Member::Member(){
    name = "";
    street_address = "";
    city = "";
    state ="";
    zipcode = 0;
    id = 0;
    status = false;

}

Member::~Member()
{
    name.clear();
    street_address.clear();
    city.clear();
    state.clear();
    zipcode = 0;
    id = 0;
    status = false;
}

int Member::get_id(){
    return id;
}
int Member::compare_id(int comp_id){
    if(comp_id == id)
        return 1;

    return 0;
}
int Member::add_member(string add_name, string add_street, string add_city, string add_state,
                        int zip, int add_id, bool stat){
    Member mem;
	mem.name = add_name;
    mem.street_address = add_street;
    mem.city = add_city;
    mem.state = add_state;
    mem.zipcode = zip;
    mem.id = add_id;
    mem.status = stat;
    member_list.push_back(mem);
    return 1;
}

bool Member::operator==(const Member & src) const
{
    return this->id == src.id;
}

int Member::update_member(int a_member_number)
{
    bool condition = false;
    if(a_member_number == id){
        do{
            cout << "Enter in your updated name: ";
            getline(cin, name); 
            if(name.length() > MAX_NAME){
                cout << "NAME IS TOO LONG. ENTER AGAIN (25 CHARACTERS)\n\n";
                name.clear();
                condition = false;
            }
            else
                condition = true;
        }while(condition == false);
        cout << "Name updated.\n\n";

        do{
            cout << "Enter in your new steet address (25 characters): ";
            getline(cin, street_address);
            if(street_address.length() > MAX_ADDRESS){
                cout << "STREET ADDRESS TOO LONG. TRY AGAIN\n\n";
                street_address.clear();
                condition= false;
            }
            else    condition = true;

        }while(condition == false);

        cout << "Street address updated.\n\n";

        do{
            cout << "Enter in your new City (14 characters): ";
            getline(cin,city);
            if(city.length() > MAX_CITY){
                cout << "CITY NAME TOO LONG. TRY AGAIN\n\n";
                city.clear();
                condition = false;
            }
            else    condition = true;
        }while(condition == false);
        cout << "City updated.\n\n";

        do{
            cout << "Enter in new state (2): ";
            getline(cin, state);
            if(state.length() > MAX_STATE){
                cout << "STATE TOO LONG. TRY AGAIN\n\n";
                state.clear();
                condition = false;
            }
            else    condition = true;
        }while(condition == false);
        cout << "State updated.\n\n";

        do{
            cout << "Enter in new zip code (5): ";
            cin >> zipcode; cin.ignore(100,'\n');
            if(int(log10(zipcode)+1) > MAX_ZIPCODE){
                cout << "ZIP CODE MUST BE 5 DIGITS. TRY AGAIN\n\n";
                condition = false;
            }
            else  condition = true;
        }while(!condition);
        cout << "Zip code updated.\n\n";
        return 1;
    }
   
    return 0;
}
void Member::display_matching_member(int find_id){
    Member mem; //temp search variable
    mem.id = find_id;

    auto it = find(member_list.begin(), member_list.end(), mem);

    if (it != member_list.end())
        it->display_member();
    else
        cout << find_id << " could not be found.\n";
}

int Member::display_member(){
    cout << "Member name: "<<name<<endl
    << "Member ID: " << id<<endl;
    cout << "Status: ";
    if(status == 1)
        cout << "Active\n";
    else
        cout << "Unactive\n";

    cout <<"Address: "<< street_address<<endl
    <<city << ", " << state << " " << zipcode<<endl<<endl;

    return 1;
}

int Member::remove_member(int a_member_number)
{
    Member mem;
    mem.id = a_member_number;
    auto it = find(member_list.begin(), member_list.end(), mem);
    if(it != member_list.end())
    {
        member_list.erase(it);
        cout << a_member_number << " successfully deleted.\n";
        return 1;
    }
        cout << a_member_number << " could not be found. " << endl;
    return 0;
}

void Member::add_service(const services & src)
{
    service_list.push_back(src);
}

void Member::generateMemberServiceRerpot() const
{
    if(id == 0){
        cout << "UNABLE TO GENERATE REPORT. THERE ARE NO MEMBERS\n\n";
        return;
    }
    char get_date[11];
    cout << "Member name: " << name << endl;
    cout << "Member number: " << id << endl;
    cout << "Member address: " << street_address << endl;
    cout << "Member city: " << city << endl;
    cout << "Member state: " << state << endl;
    cout << "Member zipcode: " << zipcode << endl;
    cout << "Service:" << endl;
    for(const auto & i: service_list)
    {
        cout << "Date of service: " << i.getDateOfService(get_date) << endl;
        cout << "Provider name:" << i.getProviderName() << endl;
        cout << "Service name" << i.getServiceName() << endl;
        cout << "-----------------------------------" << endl;
    }
}
