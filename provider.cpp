#include "structures.h"

using std::vector;

provider::provider()
{
    name = "";	
	street_address = "";
    city = "";
    zip_code = 0;
    provider_number = 0;
}

bool provider::operator == (const provider & src) const {
	return this->provider_number == src.provider_number;
}

int provider::add_provider(string add_name, string add_street_address, string add_city, 
                            string add_state, int add_zip_code, int add_provider_number){
    provider new_pro;

    new_pro.name = add_name;
    new_pro.street_address = add_street_address;
    new_pro.city  = add_city;
    new_pro.zip_code  = add_zip_code;
    new_pro.provider_number  = add_provider_number;
    new_pro.state = add_state;
    cout << "Your Provider number is: "<<add_provider_number<<endl;
    pro_list.push_back(new_pro);
    return 1;
}

int provider::remove_provider(int provider_number){
	provider p; //temp search variable
	p.provider_number = provider_number;

	auto it = find(pro_list.begin(), pro_list.end(), p);
	
	if (it != pro_list.end()){	
		pro_list.erase(it);
		cout << provider_number << " successfully deleted.\n";
		return 1;
	}
    else
        cout << provider_number << " could not be found.\n";
    return 0;
}

int provider::update_provider(int provider_number){
    bool condition = false;
    
    for(size_t i = 0; i < pro_list.size(); ++i){
        if(pro_list[i].provider_number == provider_number){
            pro_list[i].name.clear();
            pro_list[i].street_address.clear();
            pro_list[i].city.clear();
            pro_list[i].state.clear();



            do{
                cout << "Enter in your updated name: ";
                getline(cin, pro_list[i].name); 
                if(pro_list[i].name.length() > MAX_NAME){
                    cout << "NAME IS TOO LONG. ENTER AGAIN (25 CHARACTERS)\n\n";
                    pro_list[i].name.clear();
                    condition = false;
                }
                else
                    condition = true;
            }while(condition == false);
            cout << "Name updated.\n\n";

            do{
                cout << "Enter in your new steet address (25 characters): ";
                getline(cin, pro_list[i].street_address);
                if(pro_list[i].street_address.length() > MAX_ADDRESS){
                    cout << "STREET ADDRESS TOO LONG. TRY AGAIN\n\n";
                    pro_list[i].street_address.clear();
                    condition= false;
                }
                else    condition = true;

            }while(condition == false);

            cout << "Street address updated.\n\n";

            do{
                cout << "Enter in your new City (14 characters): ";
                getline(cin,pro_list[i].city);
                if(pro_list[i].city.length() > MAX_CITY){
                    cout << "CITY NAME TOO LONG. TRY AGAIN\n\n";
                    pro_list[i].city.clear();
                    condition = false;
                }
                else    condition = true;
            }while(condition == false);
            cout << "City updated.\n\n";

            do{
                cout << "Enter in new state (2): ";
                getline(cin, pro_list[i].state);
                if(pro_list[i].state.length() > MAX_STATE){
                    cout << "STATE TOO LONG. TRY AGAIN\n\n";
                    pro_list[i].state.clear();
                    condition = false;
                }
                else    condition = true;
            }while(condition == false);
            cout << "State updated.\n\n";

            do{
                cout << "Enter in new zip code (5): ";
                cin >> pro_list[i].zip_code; cin.ignore(100,'\n');
                if(int(log10(pro_list[i].zip_code)+1) > MAX_ZIPCODE){
                    cout << "ZIP CODE MUST BE 5 DIGITS. TRY AGAIN\n\n";
                    condition = false;
                }
                 else  condition = true;
            }while(!condition);
            cout << "Zip code updated.\n\n";
            return 1;
        }
    }
    cout << "PROVIDER WITH THAT ID DOES NOT EXIST. UPDATE FAILED.\n\n";
    return 0;



}

void provider::display_provider(){

        cout << "Provider name: " << name << endl;
        cout << "Provider number: " << provider_number << endl;
        cout << "Provider street address: " << street_address << endl;
        cout << "Provider city: " << city << endl;
        cout << "Provider state: " << state << endl;
        cout << "Provider zip code: " << zip_code << endl;
}

//search for provider using id, then display
void provider::display_provider(int find_id){
    provider p; //temp search variable
	p.provider_number = find_id;

	auto it = find(pro_list.begin(), pro_list.end(), p);
	
	if (it != pro_list.end())
        it->display_provider();
}

int provider::find_provider(int find_id){
    provider p; //temp search variable
	p.provider_number = find_id;

	auto it = find(pro_list.begin(), pro_list.end(), p);
	
	if (it != pro_list.end()){
        return it->provider_number;
        return 0;
    }
    cout << find_id << " could not be found.\n";
    return -1;    
}

void provider::display_all_provider(){
    for (auto i = pro_list.begin(); i != pro_list.end(); ++i){
        i->display_provider();    
        cout << '\n';
    }   
}

//generate provider report to be emailed to each provider
void provider::generate_provider_report(int provider_number) const
{
    provider p; //temp search variable
	p.provider_number = provider_number;

	auto it = find(pro_list.begin(), pro_list.end(), p);
	
	if (it != pro_list.end()){
        char get_date[11];
        cout << "Provider name: " << it->name << endl;
        cout << "Provider number: " << it->provider_number << endl;
        cout << "Provider street address: " << it->street_address << endl;
        cout << "Provider city: " << it->city << endl;
        cout << "Provider state: " << it->state << endl;
        cout << "Provider zipcode: " << it->zip_code << endl;
        cout << "Services provided:" << endl;
        for(const auto & i: service_list)
        {
            cout << "Date of service: " << i.getDateOfService(get_date) << endl;
            //Date and time data were received by the computer (MM-DD-YYYY HH:MM:SS)
            cout << "Member name:" << i.getMemberName() << endl;
            cout << "Member number: " << i.getMembernumber() << endl;
            cout << "Service code: " << i.getServiceCode() << endl;
            cout << "Fee: " << i.getFee() << endl;
            //Total number of consultations with members (3 digits).
            //Total fee for the week (up to $99,999.99).
            cout << "-----------------------------------" << endl;
        }
    }
    else
        cout << provider_number << " could not be found.\n";   
}

void provider::add_service(const services & src)
{
    service_list.push_back(src);
}