#include "structures.h"

services::services()
{
    service_name = "";	
    service_date = nullptr;
    member_name = "";
    provider_name = "";
    member_number = 0;
    provider_number = 0;
    code = 0;
    fee = 0.0f;

}

services::~services()
{
    service_name.clear();
    if(service_date)
	delete [] service_date;
    service_date = nullptr;
    member_name.clear();
    provider_name.clear();
    member_number = 0;
    provider_number = 0;
    code = 0;
    fee = 0.0f;
}

services::services(const string a_service_name, char a_service_date[], const string a_member_name, const string a_provider_name, 
			int a_member_number, int a_provider_number, int a_code, float a_fee):
			service_name(a_service_name),  member_name(a_member_name),
			provider_name(a_provider_name), member_number(a_member_number), provider_number(a_provider_number), code(a_code), fee(a_fee)
{
	service_date = new char[strlen(a_service_date) + 1];
	strcpy(service_date, a_service_date);
}

services::services(const services & copy)
{
    service_name = copy.service_name;
    service_date = new char[strlen(copy.service_date) + 1];
    strcpy(service_date, copy.service_date);
    member_name = copy.member_name;
    provider_name = copy.provider_name;
    member_number = copy.member_number;
    provider_number = copy.provider_number;
    code = copy.code;
    fee = copy.fee;
}

char * services::getDateOfService(char get_date[]) const
{
    get_date = new char[strlen(service_date) + 1];
    strcpy(get_date, service_date);
    return get_date;
}

string services::getMemberName() const
{
    return member_name;
}

string services::getProviderName() const
{
    return provider_name;
}

string services::getServiceName() const
{
    return service_name;
}

int services::getMembernumber() const
{
    return member_number;
}

int services::getServiceCode() const
{
    return code;
}

int services::getFee() const
{
    return fee;
}

int services::getProvidernumber() const
{
    return provider_number;
}

int services::remove(int code_comp){
    if(code_comp == code){
        remove_all();
        cout << "SERVICE INFO HAS BEEN REMOVED\n\n";
        return 1;
    }
    cout << "The service code you enter doesn't exist" << endl;
    return 0;
}
int services::remove_all(){
    
    service_name.clear();
    if(service_date)
   	delete [] service_date;
    service_date = nullptr;
    member_name.clear();
    provider_name.clear();
    member_number = 0;
    provider_number = 0;
    code = 0;
    fee = 0.0f;
    return 1;
}

int services::display_all(){
    cout << "Service name: " << service_name << endl
        << "Service date: " << service_date << endl
        << "Member name: " << member_name << endl
        << "Member number: " << member_number << endl
        << "Provider name: " << provider_name << endl
        << "Provider number: " << provider_number << endl
        << "Service Code: " << code << endl
        << "Fee: $" << fee << endl << endl;

    return 1;}

int services::display_certain(int code_comp){
    if(code_comp == code){
        cout << "Service Name: " << service_name << endl
            << "Service Date: " << service_date << endl
            << "Member name: " << member_name << endl
            << "Member number: " << member_number << endl
            << "Provider name:" << provider_name << endl
            << "Provider number: " << provider_number << endl
            << "Service Code: " << code << endl
            << "Fee: $" << fee << endl << endl;
            return 1;
    }
    cout << "The service code you enter doesn't exist" << endl;
    return 0;
}

bool services::services_compare(const services & src)
{
    if(this == &src)
    {
        return true;
    }
    else
        return false;
}
services & services::operator =(const services & src)
{
    try
    {
        if(this == &src)
        {
            throw 10;
        }
    }
    catch(int x)
    {
        cout << "can not copy itself." << endl;
    }
    service_name.clear();
    if(service_date)
    	delete [] service_date;
    member_name.clear();
    provider_name.clear();
    service_name = src.service_name;
    service_date = new char[strlen(src.service_date) + 1];
    strcpy(service_date, src.service_date);
    member_name = src.member_name;
    provider_name = src.provider_name;
    member_number = src.member_number;
    provider_number = src.provider_number;
    code = src.code;
    fee = src.fee;
    return *this;
}
bool services::operator==(const services & src) const
{
    return !(service_name == src.service_name && strcmp(service_date, src.service_date) == 0 && 
    member_name == src.member_name && provider_name == src.provider_name && 
    member_number == src.member_number && provider_number == src.provider_number && code == src.code && fee == src.fee);
}
