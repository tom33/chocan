#include <cctype>
#include <fstream>
#include <cmath>
#include <algorithm> //std::find
#include <vector>
#include <cstring>
#include <ctime>
#include <iostream>
#include <iomanip>

using namespace std;

float const BLOOD_TEST = 49.99f;
float const AEROBICS = 199.99f;
float const DIETITIAN = 99.99f;
float const INTERNISTS = 199.99f;
int const MAX_SERVICE_NAME = 20;
int const MAX_NAME = 25;
int const range_max = 999999999;
int const range_min = 100000000;
int const code_range_max = 999999;
int const code_range_min = 100000;
int const MAX_ID = 9;
int const MAX_ADDRESS = 25;
int const MAX_CITY = 14;
int const MAX_STATE = 2;
int const MAX_ZIPCODE = 5;
int const MAX_COMMENTS = 100;
int const MAX_SERVICE_CODE = 6;
float const MAX_TOTALFEE = 99999.99;
float const MAX_FEE = 999.99;

class services{
	public:
		services();
		~services();
		services(const string, char[], const string, const string, int, int, int, float);
		services(const services & copy);
		int remove_all();
		int remove(int);
		int update();//unsure if this is useless or not
		int display_all();
		int display_certain(int);
		bool operator == (const services & src) const;
		bool services_compare(const services &);
		services & operator=(const services & src);
		char * getDateOfService(char get_date[]) const;
		string getProviderName() const;
		string getMemberName() const;
		string getServiceName() const;
		int getMembernumber() const;
		int getServiceCode() const;
		int getFee() const;
		int getProvidernumber() const;
	private:
		string service_name;	
		char * service_date;
		string member_name;
		string provider_name;
		int member_number;
		int provider_number;
		int code;
		float fee;
		vector<services> service_list;
};

class provider{
	public:
		provider();
 		bool operator == (const provider & src) const;
		int add_provider(string name, string street_address, string city, string state, int zip_code, int provider_number);
		int remove_provider(int provider_number);
		int update_provider(int provider_number);
		void display_provider();
		void display_all_provider();
		void generate_provider_report(int provider_numder) const;
		void add_service(const services & src);
		int find_provider(int);
		void display_provider(int);
	private:
		string name;
		string street_address;
		string city;
		string state;
		int zip_code;
		int provider_number;
		vector<provider> pro_list;
		vector<services> service_list;
		//map<string, int> provider_directory;

};

class Member
{
	public:
		Member(const string, const string, const string, const string,
				int , int, bool);
		~Member();
		Member();
		int add_member( string,  string, string, string, int, int, bool);
		int update_member(int);
		int display_member();
		
		void display_matching_member(int);
		int remove_member(int);
		void add_service(const services & src);
		void generateMemberServiceRerpot() const;
		int compare_id(int);
		bool operator == (const Member & src) const;
		int get_id();
	private:
		string name;
		string street_address;
		string city;
		string state;
		int zipcode;
		int id;
		bool status;
		vector<Member> member_list;
		vector<services> service_list;

};

class Billing
{
	public:
		Billing();
		~Billing();
		int create_billing(string date, int pro_num, int mem_num, int code, string new_comment);
		int remove_matching_billing();
		int remove_all();
		int display_all();
		int display_matching_billing();
	private:
	//	int billing_id;
	//	int consultation_id;
	//	double amount;
	//	bool billing_status;
	string service_date;
	char currentDate[11];
	int provider_number;
	int member_number;
	int service_code;
	string comment;
};

class Consultation
{
	public:
		Consultation();
		~Consultation();
		Consultation(char[], char[], char[], int, int , int);
		Consultation(const Consultation & copy);
		int record_to_disk(const Consultation & src);
		int read_from_disk(char[], char[], char[], int , int, int);
		Consultation & operator=(const Consultation & src);
	private:
		char * current_date;
		char * service_date;
		char * comments;
		int member_number;
		int provider_number;
		int code;
		vector<Consultation> Consultation_list;
};
